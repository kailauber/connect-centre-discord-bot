import json
import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
from cogs.moderation import Moderation
from cogs.utility import Utility
from cogs.verification import Verification

load_dotenv()

client = commands.Bot(command_prefix='!', intents=discord.Intents.all())

@client.event
async def on_ready():
    await client.add_cog(Moderation(client))
    await client.add_cog(Utility(client))
    await client.add_cog(Verification(client))
    print(f"{client.user.name} has connected to Discord!")

# Set up custom help command with embed and formatting
class CustomHelpCommand(commands.DefaultHelpCommand):
    def __init__(self):
        super().__init__()

    async def send_bot_help(self, mapping):
        embed = discord.Embed(title="Help", description="List of available commands:")
        for cog, commands in mapping.items():
            if cog:
                name = cog.qualified_name
                filtered = await self.filter_commands(commands, sort=True)
                if filtered:
                    value = '\n'.join(f"`{c.name}` - {c.help}" for c in filtered)
                    embed.add_field(name=name, value=value, inline=False)
            else:
                filtered = await self.filter_commands(commands, sort=True)
                if filtered:
                    value = '\n'.join(f"`{c.name}` - {c.help}" for c in filtered)
                    embed.add_field(name="No Category", value=value, inline=False)
        channel = self.get_destination()
        await channel.send(embed=embed)

@client.event
async def on_message(message):
    if message.guild:
        await client.process_commands(message)

client.remove_command('help')
client.help_command = CustomHelpCommand()
client.run(os.getenv('BOT_TOKEN'))
