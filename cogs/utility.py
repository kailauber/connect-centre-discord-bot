import discord
from discord.ext import commands

class Utility(commands.Cog):
    def __init__(self, client):
        self.client = client
        print("Utility Cog initialized")

    @commands.command()
    async def ping(self, ctx):
        """Responds with the bot's latency in milliseconds."""
        await ctx.send(f'Pong! {round(self.client.latency * 1000)}ms')

    @commands.command()
    async def say(self, ctx, *, message=None):
        """Repeats a message."""
        if message is None:
            await ctx.send('Please provide a message for the bot to say.')
            return
        try:
            await ctx.send(message)
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to send the message.')

