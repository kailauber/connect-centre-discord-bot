import discord
from discord.ext import commands
import json
import os

class Moderation(commands.Cog):
    def __init__(self, client):
        self.client = client
        print("Moderation Cog initialized")

    # Set the file path for warnings.json
    warnings_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'warnings.json')

    # Load warnings from file
    warnings_dict = {}
    try:
        with open(warnings_file_path) as file:
            warnings_dict = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        pass

    @commands.command()
    async def warn(self, ctx, member: discord.Member = None, *, reason=None):
        """Sends a warning message to a member."""
        if member is None:
            await ctx.send('Please mention a member to warn.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the warning.')
            return
        if str(member.id) not in self.warnings_dict:
            self.warnings_dict[str(member.id)] = []
        self.warnings_dict[str(member.id)].append(reason)
        await ctx.send(f'{member.mention}, you have been warned for {reason}.')

        # Update the warnings file
        with open(self.warnings_file_path, 'w') as file:
            json.dump(self.warnings_dict, file)

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.Member = None, *, reason=None):
        """Kicks a member from the server."""
        if member is None:
            await ctx.send('Please mention a member to kick.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the kick.')
            return
        try:
            await member.kick(reason=reason)
            await ctx.send(f'{member.mention} has been kicked for {reason}.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to kick members.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to kick the member.')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.Member = None, *, reason=None):
        """Bans a member from the server."""
        if member is None:
            await ctx.send('Please mention a member to ban.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the ban.')
            return
        try:
            await member.ban(reason=reason)
            await ctx.send(f'{member.mention} has been banned for {reason}.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to ban members.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to ban the member.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def mute(self, ctx, member: discord.Member = None, *, reason=None):
        """Mutes a member in the server and gives a warning."""
        if member is None:
            await ctx.send('Please mention a member to mute.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the mute.')
            return
        try:
            # Find the "Muted" role in the server
            muted_role = discord.utils.get(ctx.guild.roles, name="Muted")

            # If the "Muted" role does not exist, create it
            if not muted_role:
               
                muted_role = await ctx.guild.create_role(name="Muted")

                # Set the permissions for the "Muted" role
                for channel in ctx.guild.channels:
                    await channel.set_permissions(muted_role, send_messages=False)

            # Add the "Muted" role to the member
            await member.add_roles(muted_role, reason=reason)

            # Give a warning to the member
            if str(member.id) not in self.warnings_dict:
                self.warnings_dict[str(member.id)] = []
            self.warnings_dict[str(member.id)].append(reason)
            await ctx.send(f'{member.mention}, you have been warned for {reason} and muted.')

            # Update the warnings file
            with open(self.warnings_file_path, 'w') as file:
                json.dump(self.warnings_dict, file)

        except discord.Forbidden:
            await ctx.send('I do not have permission to manage roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to mute the member.')

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clearwarn(self, ctx, member: discord.Member = None, index_or_all: str = None):
        """Clears a specific warning of a member by its index or all warnings."""
        if member is None:
            await ctx.send('Please mention a member.')
            return

        if str(member.id) not in self.warnings_dict:
            await ctx.send(f'{member.mention} has no warnings.')
            return

        warnings = self.warnings_dict[str(member.id)]

        if index_or_all == "all":
            warnings.clear()
            await ctx.send(f"All warnings for {member.mention} have been cleared.")
        elif index_or_all is not None and index_or_all.isdigit():
            warning_index = int(index_or_all)
            if warning_index < 1 or warning_index > len(warnings):
                await ctx.send('Invalid warning index. Please provide a valid index number.')
                return

            # Remove the warning from the list
            removed_warning = warnings.pop(warning_index - 1)
            await ctx.send(f"Warning {warning_index} for {member.mention} has been removed: '{removed_warning}'")
        else:
            await ctx.send("Please provide a valid index number or 'all' to clear all warnings.")

        # Update the warnings file
        with open(self.warnings_file_path, 'w') as file:
            json.dump(self.warnings_dict, file)

    @commands.command()
    async def warnings(self, ctx, *, arg=None):
        """Displays all warnings a user has. If no member is provided, shows your own warnings."""

        if arg == "all" and ctx.author.guild_permissions.administrator:
            all_warnings = []
            for member_id, warnings in self.warnings_dict.items():
                member = ctx.guild.get_member(int(member_id))
                if member:
                    warning_count = len(warnings)
                    warning_list = "\n".join(f"{i+1}. {warning}" for i, warning in enumerate(warnings))
                    all_warnings.append(f'{member.mention} has {warning_count} warning(s):\n{warning_list}')
            if all_warnings:
                await ctx.send("\n\n".join(all_warnings))
            else:
                await ctx.send("No warnings found for any member.")
            return

        if arg is None:
            member = ctx.author
        else:
            try:
                member = await commands.MemberConverter().convert(ctx, arg)
            except commands.MemberNotFound:
                await ctx.send('Member not found. Please provide a valid member or use "all" to show warnings for all members.')
                return

        if str(member.id) not in self.warnings_dict:
            await ctx.send(f'{member.mention} has no warnings.')
        else:
            warnings = self.warnings_dict[str(member.id)]
            warning_count = len(warnings)
            if warning_count == 0:
                await ctx.send(f'{member.mention} has no warnings.')
            else:
                warning_list = "\n".join(f"{i+1}. {warning}" for i, warning in enumerate(warnings))
                await ctx.send(f'{member.mention} has {warning_count} warning(s):\n{warning_list}')
