import discord
from discord.ext import commands

class Verification(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.verified_role_id = None
        print("Verification Cog initialized")

    @commands.command()
    async def verify(self, ctx):
        """Sends a verification form to the user."""
        verification_form = "Please fill out the following verification form:\nName:\nEmail:\nPhone number:\n"
        await ctx.author.send(verification_form)

        # Define a check function to ensure that the response is a DM from the same user
        def check(message):
            return isinstance(message.channel, discord.DMChannel) and message.author == ctx.author

        # Wait for the user to send the verification form back in DMs
        response = await self.bot.wait_for('message', check=check)

        # Forward the verification form to the verification channel
        verification_channel = discord.utils.get(ctx.guild.channels, name='verification')
        if not verification_channel:
            verification_channel = await ctx.guild.create_text_channel('verification')
        verification_message = await verification_channel.send(f"New verification form from {response.author.name}:\n{response.content}")

        # Add reaction emojis to the message for the verification manager to accept or decline the verification
        await verification_message.add_reaction("✅")
        await verification_message.add_reaction("❌")

        # Define a check function to ensure that the reaction is added by the verification manager
        def check_reaction(reaction, user):
            return user.top_role.name == 'Verification Manager' and reaction.message.id == verification_message.id

        # Wait for the verification manager to react to the verification message
        reaction, user = await self.bot.wait_for('reaction_add', check=check_reaction)

        # Add or remove the Verified role depending on the reaction
        if str(reaction.emoji) == "✅":
            await response.author.send("Verification has been accepted. Welcome to the server!")
            if ctx.guild:
                verified_role = discord.utils.get(ctx.guild.roles, name='Verified')
                await ctx.author.add_roles(verified_role)
        elif str(reaction.emoji) == "❌":
            await response.author.send("Verification has been declined. Please contact the server administrator for more information.")
            if ctx.guild:
                await ctx.author.kick(reason="Verification declined")

        # Delete the verification message
        await verification_message.delete()

    @commands.Cog.listener()
    async def on_ready(self):
        await self.add_roles()

    async def add_roles(self):
        # Get the Verified role
        if self.bot.guilds:
            guild = self.bot.guilds[0]
            self.verified_role_id = await self.get_verified_role(guild)
            if not self.verified_role_id:
                verified_role = await guild.create_role(name='Verified')
                self.verified_role_id = verified_role.id

    async def get_verified_role(self, guild):
        verified_role = discord.utils.get(guild.roles, name='Verified')
        if not verified_role:
            return None
        return verified_role.id
